
outer_dia = 60;
motor_dia = 52.5;
bracket_height = 48;
motor_centre_from_z = motor_dia / 2 + 6; 
back_plate_x = 54;
back_plate_y = 13;
back_plate_z = 64;


module zaxis_20x60(){
    
    difference(){
        
        union(){
            cube([60, 20, 120], center=true);
        
        }

        union() {
        
            translate([0, 0, -120])
                tslot_3d(241);
            
            translate([-20, 0, -120])
                tslot_3d(241);

            translate([20, 0, -120])
                tslot_3d(241);

        }

    }

}

module tslot_2d(){
    union(){

        polygon([
            [-3.05, 0], [+3.05, 0], [+5.5, 3], 
            [+5.5, 4.25], [-5.5, 4.25], [-5.5, 3]
        ]);

        polygon([
            [+3.0, 0], [-3.0, 0],
            [-3.1, 10], [+3.0, 10]   
        ]);

    }
}

module tslot_3d(length){

    linear_extrude(length){                
        translate([0,-4,0])
            rotate([0,0,180])
                tslot_2d();
    }

}

module tslot_rail_3d(length){

    linear_extrude(length){                
        translate([0,-4,0])
            rotate([0,0,180])
                scale([0.8, 0.8, 1])
                    tslot_2d();
    }

}


module back_plate(){

    union(){

        hull(){
            translate([-back_plate_x/2, 0, 0])
                cylinder(r=back_plate_y / 2, h=back_plate_z);
            translate([back_plate_x/2, 0, 0])
                cylinder(r=back_plate_y / 2, h=back_plate_z);
        }
        
//        cube([back_plate_x,back_plate_y, back_plate_z], center=true);
    
    }
}



module m4_bolt(){

    union(){
        
        cylinder(h=26, r=2, $fn=32);
        cylinder(h=6, r=4, $fn=32);
        
    }
}


module rails(length){
   
    translate([-20,0,0])
        tslot_rail_3d(length);
   
    tslot_rail_3d(length);
    
    translate([20,0,0])
        tslot_rail_3d(length);

}


module clamp(){
    
    w = (outer_dia - motor_dia) / 2;
    
    hull(){
        translate([0,w/2,0])
            cylinder(r=w/2, h=bracket_height, $fn=30);

        translate([0,w/2 - 10,0])
            cylinder(r=w/2, h=bracket_height, $fn=30);
    }
}


module spindle_holder() {

    difference(){
        
        union()  {
            
            translate([0, 0, back_plate_z - bracket_height])
                cylinder(r=outer_dia/2, h=bracket_height, $fn=30);
            
            translate([0, outer_dia / 2 + back_plate_y / 2 - 8, 0]) {
                back_plate();
            }

            translate(
                [0, outer_dia / 2 + back_plate_y + 2, back_plate_z - bracket_height ]
            ) {
                rails(bracket_height);
            }

            translate([-3, -outer_dia / 2, back_plate_z - bracket_height ]) 
                clamp();
            
            translate([+3, -outer_dia / 2, back_plate_z - bracket_height ]) 
                clamp();
            
            
        }
        
        union() {
            
            translate([0,0,-100])
                cylinder(r=motor_dia/2, h=bracket_height + 120, $fn=120);
            
            translate([-1.5, - motor_dia, back_plate_z - bracket_height - 2])
                cube([3, 40, bracket_height + 4], center=false);
            
            
            translate([-10, - motor_dia / 2 - 8, back_plate_z - bracket_height]) {
                
                translate([0, 0, bracket_height * 0.15])
                    rotate([0,90,0])
                        rotate([0,0,30])
                            cylinder(r=2.1, h= 20);

                translate([0, 0, bracket_height * 0.5])
                    rotate([0,90,0])
                        rotate([0,0,60])
                            cylinder(r=2.1, h= 20);

                translate([0, 0, bracket_height * 0.85])
                    rotate([0,90,0])
                        rotate([0,0,60])
                            cylinder(r=2.1, h= 20);

            }

            translate([-20, 15, 10])
                rotate([-90,0,0]){
                     rotate([0,0,60])
                        cylinder(r=2.1, h= 30);
                }

            translate([20, 15, 10])
                rotate([-90,0,0]){
                    rotate([0,0,60])
                        cylinder(r=2.1, h= 30);
            }
        
            
        }
            
    }
}


spindle_holder();

translate([0, motor_centre_from_z + 13, -30])    
//    color("silver")
        %zaxis_20x60();

translate([-20, 15, 10])
    rotate([-90,0,0]){
         %m4_bolt();
    }

translate([20, 15, 10])
    rotate([-90,0,0]){
        %m4_bolt();
}


